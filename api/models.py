from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


class User(AbstractUser):
    username = models.CharField(max_length=255, blank=True, null=True, unique=True)
    email = models.EmailField(_('email_address'), unique=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def __str__(self):
        return '{}'.format(self.username)


class Customer(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='customer')
    photo = models.ImageField(upload_to='uploads', blank=True)


class Rol(models.Model):
    description = models.CharField(max_length=255, blank=True, null=True)


class Employee(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='employee')
    rol = models.ForeignKey(Rol, related_name='rol', on_delete=models.DO_NOTHING)
    address = models.CharField(max_length=255, blank=True, null=True)
    birthday = models.DateField()
    phone_number = models.CharField(max_length=255, blank=True, null=True)
    salary = models.FloatField()
    ine = models.CharField(max_length=255, blank=True, null=True)
