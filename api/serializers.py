from rest_framework import serializers
from api.models import User, Customer, Employee


class UserCustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ('photo',)


class UserEmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = ('address', 'birthday', 'phone_number', 'salary', 'ine')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    customer = UserCustomerSerializer(required=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name', 'password', 'customer')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        customer_data = validated_data.pop('customer')
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        Customer.objects.create(user=user, **customer_data)
        return user

    def update(self, instance, validated_data):
        customer_data = validated_data.pop('customer')
        customer = instance.customer

        instance.username = validated_data.get('username', instance.username)
        instance.save()

        customer.photo = customer_data.get('photo', customer.photo)
        customer.save()

        return instance


class EmployeeSerializer(serializers.HyperlinkedModelSerializer):
    employee = UserEmployeeSerializer(required=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name', 'password', 'employee')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        employee_data = validated_data.pop('employee')
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        Employee.objects.create(user=user, **employee_data)
        return user

    def update(self, instance, validated_data):
        employee_data = validated_data.pop('employee')
        employee = instance.employee

        instance.employee = validated_data.get('username', instance.username)
        instance.save()

        employee.address = employee_data.get('address', employee.address)
        employee.birthday = employee_data.get('birthday', employee.birthday)
        employee.phone_number = employee_data.get('phone_number', employee.phone_number)
        employee.salary = employee_data.get('salary', employee.salary)
        employee.ine = employee_data.get('ine', employee.ine)
        employee.save()

        return instance

